WordClock
=========

Arduino based project to create a LED clock, displaying the current time using Dutch words.

# Hardware
 * ATmega328 (arduino)
 * WS2801 based strip with 5050 SMD RGB LEDs
 * LDR
 * DCF77

# Software
## Libraries
 * [FastSPI](https://code.google.com/p/fastspi/)
 * [DCF77](https://github.com/thijse/Arduino-Libraries/tree/master/DCF77)

# TODO
 * use fastpi2
 * use passive dcf77 sketch