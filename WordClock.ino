#include <FastSPI_LED.h>

#include "DCF77.h"
#include "Time.h"

#define DCF_PIN 2         // Connection pin to DCF 77 device
#define DCF_INTERRUPT 0   // Interrupt number associated with pin
#define NUM_LEDS 114      // # of leds used in total
int LDR_Pin = A0;         // LDR on analog pin 0

time_t time;
DCF77 DCF = DCF77(DCF_PIN,DCF_INTERRUPT);

byte ledIntensity = 255;

struct CRGB { unsigned char b; unsigned char g; unsigned char r; }; // LED strip wiring: Sometimes chipsets wire in an other sort of way
struct CRGB *leds;

byte prevMinute = 0;
const byte byteSize = sizeof(byte);

// QClock words definitions
const byte EEN[]    = {34,  25, 14};
const byte TWEE[]   = {104, 95, 84, 75};
const byte DRIE[]   = {35,  24, 15, 4};
const byte VIER[]   = {103, 96, 83, 76};
const byte VIJF[]   = {63,  56, 43, 36};
const byte ZES[]    = {23,  16, 3};
const byte ZEVEN[]  = {102, 97, 82, 77, 62};
const byte ACHT[]   = {101, 98, 81, 78};
const byte NEGEN[]  = {42,  37, 22, 17, 2};
const byte TIEN[]   = {61,  58, 41, 38};
const byte ELF[]    = {21,  18, 1};
const byte TWAALF[] = {100, 99, 80, 79, 60, 59};

const byte UUR[]    = {20, 19, 0};

const byte TIEN_T[] = {108, 91, 88, 71};
const byte VIJF_T[] = {30,  29, 10, 9};
const byte VOOR1[]  = {31,  28, 11, 8};
const byte OVER1[]  = {107, 92, 87, 72};
const byte KWART[]  = {47,  32, 27, 12, 7};
const byte HALF[]   = {106, 93, 86, 73};
const byte OVER2[]  = {33,  26, 13, 6};
const byte VOOR2[]  = {105, 94, 85, 74};

// corner minute leds
const byte oneMinute[]    = {110};
const byte twoMinutes[]   = {110, 111};
const byte threeMinutes[] = {110, 111, 112};
const byte fourMinutes[]  = {110, 111, 112, 113};

/****************
* MERGED ARRAYS *
*****************/
const byte HET_IS[]             = {109, 90, 89, 69, 50};
const byte VIJF_T_OVER2[]       = {30, 29, 10, 9, 33, 26, 13, 6};
const byte TIEN_T_OVER1[]       = {108, 91, 88, 71, 107, 92, 87, 72};
const byte KWART_OVER2[]        = {47, 32, 27, 12, 7, 33, 26, 13, 6};
const byte TIEN_T_VOOR1_HALF[]  = {108, 91, 88, 71, 31, 28, 11, 8, 106, 93, 86, 73};
const byte VIJF_T_VOOR1_HALF[]  = {30, 29, 10, 9, 31, 28, 11, 8, 106, 93, 86, 73};
const byte VIJF_T_OVER1_HALF[]  = {30, 29, 10, 9, 107, 92, 87, 72, 106, 93, 86, 73};
const byte TIEN_T_OVER1_HALF[]  = {108, 91, 88, 71, 107, 92, 87, 72, 106, 93, 86, 73};
const byte KWART_VOOR2[]        = {47, 32, 27, 12, 7,  105, 94, 85, 74};
const byte TIEN_T_VOOR1[]       = {108, 91, 88, 71, 31, 28, 11, 8};
const byte VIJF_T_VOOR2[]       = {30, 29, 10, 9, 105, 94, 85, 74};

void setup(void) {
  Serial.begin(9600);
  DCF.Start();
  
  // setup led library
  FastSPI_LED.setLeds(NUM_LEDS);
  FastSPI_LED.setChipset(CFastSPI_LED::SPI_WS2801);
  FastSPI_LED.setDataRate(2);
  FastSPI_LED.init();
  FastSPI_LED.start();
  leds = (struct CRGB*)FastSPI_LED.getRGBData(); 
  memset(leds, 0, NUM_LEDS * 3);

  // light up 4 minute leds during time sync
  showLeds(fourMinutes, 4);
  FastSPI_LED.show();
  
  time_t DCFtime = DCF.getTime(); // Check if new DCF77 time is available
  while (DCFtime == 0) {
    Serial.println(F("No sync"));
    digitalClockDisplay();
    delay(1000);
    DCFtime = DCF.getTime();
  } 

  setTime(DCFtime);
  delay(3000);

  Serial.println(F("Sync"));

  // fade out minute leds (TODO not needed? reset of all leds before word show)
  for(byte k = ledIntensity; k > 0; k--) { 
    leds[110].b = k; 
    leds[110].g = k; 
    leds[110].r = k; 

    leds[111].b = k; 
    leds[111].g = k; 
    leds[111].r = k;
  
    leds[112].b = k; 
    leds[112].g = k; 
    leds[112].r = k;
  
    leds[113].b = k; 
    leds[113].g = k; 
    leds[113].r = k;    
    
    FastSPI_LED.show();
    delay(3);
  }
}

void loop(void) {

  // readLDR
  updateLedIntensity();

  // update satelite time signal
  updateTime(); 
  
  // print hour to serial monitor
  //digitalClockDisplay();
  
  // print words on clock
  showTime();
  
  // no need to check more frequent
  delay(1000);
}

void updateLedIntensity() {
  int LDRReading = analogRead(LDR_Pin); 
  //Serial.println("LDR:");
  //Serial.println(LDRReading);
  if(LDRReading > 700) {
    ledIntensity = 255;
  } else if(LDRReading > 500){
    ledIntensity = 200;
  } else if(LDRReading > 300) {
    ledIntensity = 100;
  } else {
    ledIntensity = 50;
  }
}

void updateTime() {
  time_t DCFtime = DCF.getTime();
  if (DCFtime!=0) {
    Serial.println("Time is updated");
    setTime(DCFtime);
  } 
}

void showTime() {
  // reset all leds
  memset(leds, 0, NUM_LEDS * 3);

  showMinuteWords();
  showHour();
  showMinutCorners();

  FastSPI_LED.show();
}

void showMinuteWords() {
  showLeds(HET_IS, 5);

  if(minute() < 5) {
    showLeds(UUR, 3);
  } else if(minute() < 10) {
    showLeds(VIJF_T_OVER2, 8);
  } else if(minute() < 15) {
    showLeds(TIEN_T_OVER1, 8);
  } else if(minute() < 20) {
    showLeds(KWART_OVER2, 9);
  } else if(minute() < 25) {
    showLeds(TIEN_T_VOOR1_HALF, 12);
  } else if(minute() < 30) {
    showLeds(VIJF_T_VOOR1_HALF, 12);
  } else if(minute() < 35) {
    showLeds(HALF, 4);
  } else if(minute() < 40) {
    showLeds(VIJF_T_OVER1_HALF, 12);
  } else if(minute() < 45) {
    showLeds(TIEN_T_OVER1_HALF, 12);
  } else if(minute() < 50) {
    showLeds(KWART_VOOR2, 9);
  } else if(minute() < 55) {
    showLeds(TIEN_T_VOOR1, 8);
  } else {
    showLeds(VIJF_T_VOOR2, 8);
  }
}

void showHour() {
  byte uur = hour();

  if(minute() > 20) {
    uur++;
  }

  if(uur == 1 || uur == 13) {
    showLeds(EEN, 3);
  } else if(uur == 2 || uur == 14) {
    showLeds(TWEE, 4);
  } else if(uur == 3 || uur == 15) {
    showLeds(DRIE, 4);
  } else if(uur == 4 || uur == 16) {
    showLeds(VIER, 4);
  } else if(uur == 5 || uur == 17) {
    showLeds(VIJF, 4);
  } else if(uur == 6 || uur == 18) {
    showLeds(ZES, 3);
  } else if(uur == 7 || uur == 19) {
    showLeds(ZEVEN, 5);
  } else if(uur == 8 || uur == 20) {
    showLeds(ACHT, 4);
  } else if(uur == 9 || uur == 21) {
    showLeds(NEGEN, 5);
  } else if(uur == 10 || uur == 22) {
    showLeds(TIEN, 4);
  } else if(uur == 11 || uur == 23) {
    showLeds(ELF, 3);
  } else if(uur == 12 || uur == 24) {
    showLeds(TWAALF, 6);
  }
}

void showMinutCorners() {
  byte mod = minute() % 5;

  if(mod == 1) {
    showLeds(oneMinute, 1);
  } else if(mod == 2) {
    showLeds(twoMinutes, 2);
  } else if(mod == 3) {
    showLeds(threeMinutes, 3);
  } else if(mod == 4) {
    showLeds(fourMinutes, 4);
  }
}

void showLeds(const byte array[], byte arrayLength) {
  for(byte i=0; i<=arrayLength-1; i++) {
    leds[array[i]].r = ledIntensity; 
    leds[array[i]].g = ledIntensity; 
    leds[array[i]].b = ledIntensity;
  }
}

/****************
* DEBUG METHODS *
*****************/
void digitalClockDisplay(){
  // digital clock display of the time
  Serial.print(hour());
  printDigits(minute());
  printDigits(second());
  //  Serial.print(" ");
  //  Serial.print(day());
  //  Serial.print(" ");
  //  Serial.print(month());
  //  Serial.print(" ");
  //  Serial.print(year()); 
  Serial.println(); 
}

void printDigits(int digits){
  // utility function for digital clock display: prints preceding colon and leading 0
  Serial.print(":");
  if(digits < 10)
    Serial.print('0');
  Serial.print(digits);
}

void fadeAllLeds() {
	Serial.println(F("Fade in"));

  memset(leds, 0, NUM_LEDS * 3);
  for(byte k = 20; k < ledIntensity; k++) { 
    for(byte i = 0; i < NUM_LEDS; i++ ) {

     	leds[i].b = k; 
      leds[i].g = k; 
      leds[i].r = k;
        
    }
    FastSPI_LED.show();
    delay(3);
  }

  Serial.println(F("Fade out"));
    
  for(byte k = ledIntensity; k >= 0; k--) { 
   for(byte i = 0; i < NUM_LEDS; i++ ) {
 
      leds[i].b = k; 
      leds[i].g = k; 
      leds[i].r = k; 
        
    }
    FastSPI_LED.show();
    delay(3);
  }

  Serial.println(F("Done"));
}

